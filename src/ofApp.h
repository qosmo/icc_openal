#pragma once

#include "ofMain.h"
#include "ofxALSoft.h"
#include "ofxDatGui.h"
//#include "ofxFontStash.h"
#include "ofxOsc.h"

#define SOURCE_NUM 8
#define PORT 7499
#define SOURCE_DIS 1600

class ofApp : public ofBaseApp{
public:
    void setup();
    void setupGui();
    void setupIndicies();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void exit();
    
    
    void onButtonEvent(ofxDatGuiButtonEvent e);
    
protected:
    ofxALSoftContext context;
    ofxALSoftListener listener;
//    ofxALSoftMultiBuffers mBuffers;
    
    ofxALSoftBuffer buffers[SOURCE_NUM];
    ofxALSoftSource sources[SOURCE_NUM];
    ofxALSoftRAWLoader loaders[SOURCE_NUM];
    
//    ofEasyCam cam;
    
//    ofxDatGui* gui;
    
//    ofTrueTypeFont font;
    
//    ofxFontStash fontStash;
//    ofMesh mesh;
    
    ofVec3f indicies[SOURCE_NUM];
    string texts[SOURCE_NUM];
    
    bool isAuto = true;
    bool isLooping = false;
    
    ofxOscReceiver oscRec;
    ofVec3f oscListenerPos;
    ofVec3f listnerOrientation;
    
    ofxOscMessage m;
    string arg;
    string filename;
    string address;
    int index;
};
