#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofBackground(0, 0, 255, 255);
    ofEnableSmoothing();
    
    context.allocate();
    listener.allocate();
    
//    setupIndicies();
    
    oscRec.setup(PORT);
    
//    string s = "/sdsds/eeeee";
//    cout << ofSplitString(s, "/")[0] << " : " << ofSplitString(s, "/")[1] << endl;
    
//    mBuffers = ofxALSoftMultiBuffers(SOURCE_NUM);
//    mBuffers.allocate();
    
    for (int i = 0; i < SOURCE_NUM; i++) {
        buffers[i].allocate();
        sources[i].allocate();
    }
    
    string filename;
    for (int i = 0; i < SOURCE_NUM; i++) {
        filename = ofToString(i) + ".raw";
        loaders[i].load( ofToDataPath("china_news/" + filename) );
//        rawLoader.load( ofToDataPath(filename) );
//        mBuffers.buffer( loaders[i].getData(), loaders[i].getSize(), AL_FORMAT_MONO_FLOAT32, 8000 , i);
//        buffers[i].buffer( loaders[i].getData(), loaders[i].getSize(), AL_FORMAT_QUAD32, 8000 );
        buffers[i].buffer( loaders[i].getData(), loaders[i].getSize(), AL_FORMAT_MONO_FLOAT32, 8000 );
        sources[i].buffer( buffers[i]);
        sources[i].play();
        sources[i].setReferenceDistance( 20.0f );
        sources[i].setLooping( false );
        sources[i].setGain(1.);
        cout << "Buffer Size :: " << buffers[i].getSize() << endl;
    }
    
    listener.setPosition( ofVec3f(0.0f, 0.0f, 0.0f ) );
    listnerOrientation = ofVec3f( 0, 0, -1 );
    listener.setOrientation( listnerOrientation );
    listener.setUpVector( ofVec3f( 0, 1, 0 ) );
    
    // Source Position
    indicies[0] = ofVec3f(1.0f, 1.0f, 1.0f)*SOURCE_DIS;
    indicies[1] = ofVec3f(1.0f, -1.0f, 1.0f)*SOURCE_DIS;
    indicies[2] = ofVec3f(-1.0f, -1.0f, 1.0f)*SOURCE_DIS;
    indicies[3] = ofVec3f(-1.0f, 1.0f, 1.0f)*SOURCE_DIS;
    indicies[4] = ofVec3f(1.0f, 1.0f, -1.0f)*SOURCE_DIS;
    indicies[5] = ofVec3f(1.0f, -1.0f, -1.0f)*SOURCE_DIS;
    indicies[6] = ofVec3f(-1.0f, -1.0f, -1.0f)*SOURCE_DIS;
    indicies[7] = ofVec3f(-1.0f, 1.0f, -1.0f)*SOURCE_DIS;
    
    for (int i = 0; i < SOURCE_NUM; i++) {
        sources[i].setPosition(indicies[i]*2000.0);
    }
    
//    cam.setFarClip(15000);
    
//    font.load("arial.ttf", 17);
//    fontStash.loadFont("arial.ttf", 30);
    
//    setupGui();
}

//--------------------------------------------------------------


//void ofApp::setupGui() {
//    gui = new ofxDatGui( ofxDatGuiAnchor::TOP_RIGHT );
//    gui->addSlider("xSlider", -1600, 1600, 0);
//    gui->addSlider("ySlider", -1600, 1600, 0);
//    gui->addSlider("zSlider", -1600, 1600, 0);
//    gui->addButton("auto");
////    gui->addSlider("reference", 0, 100, 30);
////    gui->addSlider("gain", 0.0, 1.0, 0.5);
//    gui->setPosition(120, 0);
//    gui->getButton("auto")->onButtonEvent(this, &ofApp::onButtonEvent);
////    gui->getSlider("gain")->setLabel("Gain");
//}


void ofApp::onButtonEvent(ofxDatGuiButtonEvent e) {
    if (isAuto == true) {
        isAuto = false;
    } else {
        isAuto = true;
    }
}


//void ofApp::setupIndicies() {
//    indicies[0] = ofVec3f(0.4911, 0.3568, 0.7946);
//    indicies[1] = ofVec3f(-0.1875, 0.5773, 0.7946);
//    indicies[2] = ofVec3f(-0.6070, 0, 0.7946);
//    indicies[3] = ofVec3f(-0.1875, -0.5773, 0.7946);
//    indicies[4] = ofVec3f(0.4911, -0.3568, 0.7946);
//    indicies[5] = ofVec3f(0.7946, 0.5773, 0.1875);
//    indicies[6] = ofVec3f(-0.3035, 0.9341, 0.1875);
//    indicies[7] = ofVec3f(-0.9822, 0, 0.1875);
//    indicies[8] = ofVec3f(-0.3035, -0.9341, 0.1875);
//    indicies[9] = ofVec3f(0.7946, -0.5773, 0.1875);
//    indicies[10] = ofVec3f(0.3035, 0.9341, -0.1875);
//    indicies[11] = ofVec3f(-0.7946, 0.5773, -0.1875);
//    indicies[12] = ofVec3f(-0.7946, -0.5773, -0.1875);
//    indicies[13] = ofVec3f(0.3035, -0.9341, -0.1875);
//    indicies[14] = ofVec3f(0.9822, 0, -0.1875);
//    indicies[15] = ofVec3f(0.1875, 0.5773, -0.7946);
//    indicies[16] = ofVec3f(-0.4911, 0.3568, -0.7946);
//    indicies[17] = ofVec3f(-0.4911, -0.3568, -0.7946);
//    indicies[18] = ofVec3f(0.1875, -0.5773, -0.7946);
//    indicies[19] = ofVec3f(0.6070, 0.0, -0.7946);
//    
//    for (int i = 0; i < SOURCE_NUM; i++) {
//        mesh.addVertex(indicies[i]*2000);
//        mesh.setMode(OF_PRIMITIVE_LINES);
//    }
//    mesh.addIndex(0);
//    mesh.addIndex(1);
//    mesh.addIndex(1);
//    mesh.addIndex(2);
//    mesh.addIndex(2);
//    mesh.addIndex(3);
//    mesh.addIndex(3);
//    mesh.addIndex(4);
//    mesh.addIndex(4);
//    mesh.addIndex(0);
//    
//    mesh.addIndex(15);
//    mesh.addIndex(16);
//    mesh.addIndex(16);
//    mesh.addIndex(17);
//    mesh.addIndex(17);
//    mesh.addIndex(18);
//    mesh.addIndex(18);
//    mesh.addIndex(19);
//    mesh.addIndex(19);
//    mesh.addIndex(15);
//    
//    mesh.addIndex(0);
//    mesh.addIndex(5);
//    mesh.addIndex(1);
//    mesh.addIndex(6);
//    mesh.addIndex(2);
//    mesh.addIndex(7);
//    mesh.addIndex(3);
//    mesh.addIndex(8);
//    mesh.addIndex(4);
//    mesh.addIndex(9);
//    
//    mesh.addIndex(10);
//    mesh.addIndex(15);
//    mesh.addIndex(11);
//    mesh.addIndex(16);
//    mesh.addIndex(12);
//    mesh.addIndex(17);
//    mesh.addIndex(13);
//    mesh.addIndex(18);
//    mesh.addIndex(14);
//    mesh.addIndex(19);
//    
//    mesh.addIndex(5);
//    mesh.addIndex(10);
//    mesh.addIndex(6);
//    mesh.addIndex(11);
//    mesh.addIndex(7);
//    mesh.addIndex(12);
//    mesh.addIndex(8);
//    mesh.addIndex(13);
//    mesh.addIndex(9);
//    mesh.addIndex(14);
//    
//    mesh.addIndex(5);
//    mesh.addIndex(14);
//    mesh.addIndex(6);
//    mesh.addIndex(10);
//    mesh.addIndex(7);
//    mesh.addIndex(11);
//    mesh.addIndex(8);
//    mesh.addIndex(12);
//    mesh.addIndex(9);
//    mesh.addIndex(13);
//}

//--------------------------------------------------------------
void ofApp::update(){
    context.update();
    
    
//    osc
    while(oscRec.hasWaitingMessages()){
        oscRec.getNextMessage(&m);
        address = m.getAddress();
        if (ofSplitString(address, "/")[1] == "source") {
            index = m.getArgAsInt(0);
            arg = ofSplitString(address, "/")[2];
            if (arg == "position") {
                sources[index].setPosition(ofVec3f(m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3)));
            } else if (arg == "text") {
                texts[index] = m.getArgAsString(1);
                sources[index].pause();
                filename = ofToString(index) + ".raw";
                loaders[index].free();
                loaders[index].load( ofToDataPath(filename) );
//                ofxALSoftBuffer tmpBuffer;
//                tmpBuffer.allocate();
//                tmpBuffer.buffer( loaders[index].getData(), loaders[index].getSize(), AL_FORMAT_MONO_FLOAT32, 8000 );
//                swap(tmpBuffer, buffers[index]);
                sources[index].destroy();
                buffers[index].destroy();
                buffers[index].allocate();
                sources[index].allocate();
                buffers[index].buffer( loaders[index].getData(), loaders[index].getSize(), AL_FORMAT_MONO_FLOAT32, 8000 );
//                mBuffers.buffer( loaders[index].getData(), loaders[index].getSize(), AL_FORMAT_MONO_FLOAT32, 8000 , index);
                sources[index].buffer( buffers[index] );
//                sources[index].bufferFromBufferID( mBuffers.getALBufferID(index) );
//                sources[index].buffer(tmpBuffer);
//                tmpBuffer.destroy();
                sources[index].play();
                sources[index].setReferenceDistance( 20.0f );
                sources[index].setGain(1.0f);
            } else if (arg == "pitch") {
                sources[index].setPitch(m.getArgAsFloat(1));
//                cout << "Pitch ::: " + ofToString(m.getArgAsFloat(1)) << endl;
            } else if (arg == "reference") {
                sources[index].setReferenceDistance(m.getArgAsFloat(1));
//                cout << "Reference ::: " + ofToString(m.getArgAsFloat(1)) << endl;
            } else if (arg == "gain") {
                sources[index].setGain(m.getArgAsFloat(1));
//                cout << "Gain ::: " + ofToString(m.getArgAsFloat(1)) << endl;
            }
        } else if (ofSplitString(address, "/")[1] == "listener") {
            oscListenerPos = ofVec3f(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2));
        }
    }
    
    if (isAuto) {
//        listener.setPosition(ofVec3f(1700*cos(ofGetElapsedTimef()*0.1), 1700*sin(ofGetElapsedTimef()*0.3), 1700*cos(ofGetElapsedTimef()*0.5)));
    } else {
        listener.setPosition(oscListenerPos);
    }
    listnerOrientation.x = cos(ofGetElapsedTimef()*0.1);
    listnerOrientation.y = sin(ofGetElapsedTimef()*0.1);
    listnerOrientation.z = cos(ofGetElapsedTimef()*0.1);
    listener.setOrientation(listnerOrientation);
    listener.update();
    
    for (int i = 0; i < SOURCE_NUM; i++) {
//        sources[i].setReferenceDistance(gui->getSlider("reference")->getValue());
//        sources[i].setGain(gui->getSlider("gain")->getValue());
        sources[i].update();
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
//    ofClear( 0 );
    
//    cam.begin();
//    
//    ofSetColor(255, 130);
//    mesh.drawWireframe();
//    
//    for (int i = 0; i < SOURCE_NUM; i++) {
//        float d = ofDist(sources[i].getPosition().x,
//                         sources[i].getPosition().y,
//                         sources[i].getPosition().z,
//                         listener.getPosition().x,
//                         listener.getPosition().y,
//                         listener.getPosition().z);
//        if (d < 255.0*3.0) {
//            ofSetColor(255, (d-400)/2.0, (d-400)/2.0, 255 - (d-400)/2.0);
//            ofDrawLine(sources[i].getPosition().x,
//                       sources[i].getPosition().y,
//                       sources[i].getPosition().z,
//                       listener.getPosition().x,
//                       listener.getPosition().y,
//                       listener.getPosition().z);
//        } else {
//            ofSetColor( ofColor::white );
//        }
//        ofDrawSphere( sources[i].getPosition(), 50.0f );
//        ofPushMatrix();
//        ofPopMatrix();
//    }
//    
//    ofSetColor( ofColor::yellow );
//    
//    ofDrawSphere( listener.getPosition(), 30.0f );
//    cam.end();
    
//    ofSetColor(255, 255);
//    font.drawString("Listener X", 0, 40);
//    font.drawString("Listener Y", 0, 90);
//    font.drawString("Listener Z", 0, 140);
//    font.drawString("AUTO", 0, 190);
//    font.drawString("Reference", 0, 240);
//    font.drawString("Gain", 0, 290);
//    font.drawString("Listener", ofGetWidth()*4/5, 70);
//    font.drawString("X :: " + ofToString(listener.getPosition().x), ofGetWidth()*4/5, 100);
//    font.drawString("Y :: " + ofToString(listener.getPosition().y), ofGetWidth()*4/5, 130);
//    font.drawString("Z :: " + ofToString(listener.getPosition().z), ofGetWidth()*4/5, 160);
    
//    for (int i = 0; i < SOURCE_NUM; i++) {
//        font.drawString("Audio Source " +ofToString(i+1), ofGetWidth()*4/5, 230 + i*100);
//        font.drawString("(x, y, z) :: " + ofToString(sources[i].getPosition().x)
//                            + ", " + ofToString(sources[i].getPosition().y)
//                            + ", " + ofToString(sources[i].getPosition().z), ofGetWidth()*4/5, 260 + i*100);
//    }
    
//    if (isAuto) {
//        ofSetColor(0, 255, 0, 255);
//        font.drawString("If you set Listener Pos from OSC, please push 'AUTO' button.", 30, ofGetHeight()-30);
//    }
//    ofSetColor(255, 255);
//    font.drawString("If you change loop-state of audio, please push 'a' Button.", 10, 360);
//    if (isLooping) {
//        ofSetColor(0, 255, 0, 255);
//        font.drawString("Audio ::: Looping", 10, 400);
//    } else {
//        ofSetColor(255, 0, 0, 255);
//        font.drawString("Audio ::: Not Looping", 10, 400);
//    }
    
    ofSetWindowTitle("Frame Rate ::: " + ofToString(ofGetFrameRate()));
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == 'a') {
        if (isLooping){
            isLooping = false;
            for (int i = 0; i < SOURCE_NUM; i++) {
                sources[i].setLooping(false);
            }
        } else {
            isLooping = true;
            for (int i = 0; i < SOURCE_NUM; i++) {
                sources[i].setLooping(true);
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}


void ofApp::exit()
{
    for (int i = 0; i < SOURCE_NUM; i++) {
        buffers[i].destroy();
    }
    
    for (int i = 0; i < SOURCE_NUM; i++) {
        sources[i].destroy();
    }
    listener.destroy();
    context.destroy();
}
